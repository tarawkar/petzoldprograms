#include <Windows.h>

/*LRESULT = LONG = 32 bit
CALLBACK = __stdcall = pascal calling convention
WndProc = Windows procedure
HWND = Handle to Window receiving the msgs= 32 bit int that is given by the OS to identify our window uniquely
UINT = unsigned integer
WPARAM = WORD parameter = 16 bit in 16 bit OS = 32 bit in 32 bit OS
LPARAM = LONG parameter = 32 bit in 16 bit OS = 32 bit in 32 bit OS
*/
LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);

/*
WINAPI = __stdcall = pascal calling convention
HINSTANCE hInstance= Handle to this instance of the application.
HINSTANCE hPrevInstance = Handle to prev instance of the application.Kept for legacy purpose. Value = NULL
LPSTR = Long pointer to string.
lpszCmdLine = long pointer to zero terminated string of CommandLine Parameters
iCmdShow = Parameters of how the Window should be displayed at beginning
*/
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("HelloWin");
    HWND hWnd;
    MSG msg;
    WNDCLASS wndclass;

    //cb = count of bytes = bytesize Saves extra space in Windows class.
    wndclass.cbWndExtra = 0;
    wndclass.cbClsExtra = 0;
    
    //hbr = handle to brush. Used to paint the client area of the window. Brush is a graphics term used refer to yjr colored pattern of pixels used to fill a area
    wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);

    /*Sets icon for the window. Appears in Applications taskbar also in title bar. Use predefined icon. NULL = Default or given by OS
      When you have your own icon stored in your programs's exe file, this argument is set to hInstance.
      IDI_APPICATION = some int value representing the bitmap of application icon
    */
    wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);

    /*Sets cursor to be used when the mouse comes on the client area.*/
    wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);

    //Instance handle of the program
    wndclass.hInstance = hInstance;

    //Sets the WndProc which will process all the msgs that will be input to this window.
    wndclass.lpfnWndProc = WndProc;

    //Bit flags beacause each identifier sets a single bit in a composite value. Repaint when Horizontal or vertical size of screen changes
    wndclass.style = CS_HREDRAW|CS_VREDRAW;

    //Class menu
    wndclass.lpszMenuName = NULL;
    
    //Name if the class
    wndclass.lpszClassName = szAppName;

    /*
    This has to be done because if you are using UNICODE and Windows 98 there is no RegisterClassW in Win98. Hence this call can fail
    Hence error checking is necessary.    
    How do we know MessageBoxW will work. Coz MessageBoxW IS inplemented in Win98. Hence this code will work 
    */
    if(!RegisterClass(&wndclass))
    {
        MessageBox(NULL,TEXT("This program requires Windows NT"),szAppName,MB_ICONERROR);
        return 0;
    }

    /*Specifying detailed information about the window.
      Creates Window in memory. That means Windows OS has created a memory block to save all the info about the window to be created + some extra info
      This window does not appear on display.  
    */

   hWnd = CreateWindow(
       szAppName,                 // Window Class Name-> Associated the window with the window class
       TEXT("The Hello Program"), // Window caption
       WS_OVERLAPPEDWINDOW,       // Window style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAM | WS_MINIMIZEBOX | WS_MAXIMISE BOX       
       CW_USEDEFAULT,             // Initial x
       CW_USEDEFAULT,             // Initial y
       CW_USEDEFAULT,             // Initial width
       CW_USEDEFAULT,             // Initial height
       NULL,                      // Parent Window Handle -> OS is my parent
       NULL,                      // No menu
       hInstance,                 // Instance ID 
       NULL                       // Extra info (void*)
   );

    /*CreateWindow()
    WM_CREATE -> Sent

    ShowWindow()
    WM_SHOWWINDOW 
    WM_SIZE

    UpdateWindow()
    WM_PAINT
    
    How the window is to be initially displayed. OS kade hi value kashi ali? user selected a preference when adding the program to Start menu. Nasel tar default
    Show window puts the window on display. It also erazes the client area with background brush specified in wndclass.
    */ 
   
   ShowWindow(hWnd,iCmdShow); 
   
   //Sends WM_PAINT
   UpdateWindow(hWnd);

   /*
   struct tagMSG
   {
       HWND hwnd;       jya window la msg alay tyacha handle 
       UINT umsg;       message ID
       WPARAM wParam;   paramter for extra information of msg
       LPARAM lParam;   paramter for extra information of msg
       DWORD time;      when the msg was placed in the queue    
       POINT pt;        mouse coordinates when the msg was pushed in msg queue
   };

   struct POINT
   {
       long x;
       long y;
   };

   Getmessage -> Retrives message from Message queue.
   Call passes Windows a pointer to MSG structure. 2nd parameter - NULL coz we want msgs for all the windows created by this program
   3rd and 4th paramater 0 =  all the messages 

   Returns non-zero value for anything except WM_QUIT
   */ 

   while(GetMessage(&msg,NULL,0,0))
   {
       //Gives msg to Windows OS again for some keyboard translation
       TranslateMessage(&msg);

       /*Gives msg back to Windows OS. It then Windows calls WndProc So Control comes to application again. After executing the code for that msg cntrol goes 
       to OS again to complete the rest of dispatch message. When dispatch msg is over control comes to while loop anad again next message is processed using getmessage()*/

       DispatchMessage(&msg);
   } 

   return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
    // Device context = physical output device and its device driver. Needed to handle text and graphics in client area. So you cannot draw outside client area using hdc
    HDC hdc;

    //PAINTSTRUCT contains information that WndProc can use for painting client area.
    PAINTSTRUCT ps;

    /*
    struct RECT
    {
        LONG left;
        LONG top;
        LONG right;
        LONG bottom;
    };
    */
    RECT rect;

    switch(uMsg)
    {
        /*CreateWindow() 
        When HelloWin calls createWindow(), Windows does what it has to do and in the process, Windows calls WndProc with 1st param as hwnd and 2nd as WM_CREATE
        Use for one time initializations 

        PlaySound -> Filename , Used if sound file is a resource, 1st param is sound file | playsound should return as soon as file starts playing and continue further 
        processing

        */
        case WM_CREATE:
        PlaySound(TEXT("Hello.wav"),NULL,SND_FILENAME|SND_ASYNC);
        return 0;

        /*If we dont handle WM_PAINT it is handled by DefWindowProc as 
        BeginPaint()
        EndPaint()
        
        so that entire client area is validated.*/
        case WM_PAINT:

        /*
        BeginPaint -> Erazes background of client area if not already erazed using brush specified in hbrbackground. Hence it validates the client area.
        */
        hdc = BeginPaint(hWnd,&ps);

        /*GetClientRect initializes left top right bottom to the size of client area;
        left = top = 0
        right = width
        bottom = height

        So when you resize GetClientRect will give updated values and DrawText will draw to the center of updated value.
        */
        GetClientRect(hWnd,&rect);

        /*Drawing ahe so hdc lagnar, text , string is zero terminated,client rect,style*/
        DrawText(hdc,TEXT("Hello, Windows 98!!"),-1,&rect,DT_SINGLELINE|DT_CENTER|DT_VCENTER);

        //Releases Device Context.
        EndPaint(hWnd,&ps);

        return 0; //-> WRONG
        //break;

        //Closing the application
        case WM_DESTROY:
            //Inserts WM_QUIT msg in msg queue. GetMessage returns zero 
            PostQuitMessage(0);
            return 0; 
    }

    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}