#include <Windows.h>
#include "04-SysMetrics.h"

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("SysMets1");
    HWND hWnd;
    MSG msg;
    WNDCLASS wndclass;

    wndclass.cbWndExtra = 0;
    wndclass.cbClsExtra = 0;

    wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
    wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION),

    wndclass.hInstance = hInstance;

    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szAppName;
    wndclass.lpszMenuName = NULL;

    wndclass.style = CS_HREDRAW|CS_VREDRAW;

    if(!(RegisterClass(&wndclass)))
    {
        MessageBox(NULL,"Failed to Register class",TEXT("Error"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    hWnd = CreateWindow(
        szAppName,
        TEXT("System Metrics 1"),
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    ShowWindow(hWnd,iCmdShow);
    UpdateWindow(hWnd);

    while((GetMessage(&msg,NULL,0,0)))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
    static int cxChar,cxCaps,cyChar;
    HDC hdc;
    int i;
    PAINTSTRUCT ps;
    TCHAR szBuffer[10];
    TEXTMETRIC tm;

    switch(uMsg)
    {
        //Create Window -> Used for initialization
        case WM_CREATE:
        /*
        Accumalate all the information needed to paint a client area.
        Paint only on demand
        */
            hdc = GetDC(hWnd);

            /*Used for device independent programing*/
            GetTextMetrics(hdc,&tm);

            cxChar = tm.tmAveCharWidth;
            cxCaps = (tm.tmPitchAndFamily & 1 ? 3:2) * cxChar / 2;
            cyChar = tm.tmHeight + tm.tmExternalLeading;

            ReleaseDC(hWnd,hdc);
            return 0;
        /*
        1st time called in Update Window.
        */
        case WM_PAINT:
            hdc = BeginPaint(hWnd,&ps);

            for(i=0;i<NUMLINES;i++)
            {
                TextOut(hdc,0,cyChar*i,sysmetrics[i].szLabel,lstrlen(sysmetrics[i].szLabel));

                TextOut(hdc,22*cxCaps,cyChar*i,sysmetrics[i].szDesc,lstrlen(sysmetrics[i].szDesc));

                SetTextAlign(hdc,TA_RIGHT|TA_TOP);

                TextOut(hdc,22*cxCaps + 40 * cxChar, cyChar * i,szBuffer,wsprintf(szBuffer,TEXT("%5d"),GetSystemMetrics(sysmetrics[i].iIndex)));

                SetTextAlign(hdc,TA_LEFT|TA_TOP);

            }    

            EndPaint(hWnd,&ps);
            return 0;

        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
    }

    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

