#include <Windows.h>
#include "06-SysMetrics3.h"

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("SysMetrics3");
    HWND hWnd;
    MSG msg;
    WNDCLASS wndclass;

    wndclass.cbWndExtra = 0;
    wndclass.cbClsExtra = 0;

    wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);

    wndclass.hInstance = hInstance;

    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szAppName;
    wndclass.lpszMenuName = NULL;

    wndclass.style = CS_HREDRAW | CS_VREDRAW;

    if(!(RegisterClass(&wndclass)))
    {
        MessageBox(NULL,TEXT("Cannot register class"),TEXT("Error"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    hWnd = CreateWindow(
        szAppName,
        TEXT("System Metrics 3 - Better scrollbar"),
        WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    ShowWindow(hWnd,iCmdShow);
    UpdateWindow(hWnd);

    while(GetMessage(&msg,NULL,0,0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
    static int cxChar, cxCaps, cyChar, cxClient, cyClient, iMaxWidth;
    HDC hdc;
    int i,x,y,iVertPos,iHorzPos,iPaintBeg,iPaintEnd;
    PAINTSTRUCT ps;
    SCROLLINFO si;
    TCHAR szBuffer[10];
    TEXTMETRIC tm;

    switch(uMsg)
    {
        case WM_CREATE:
            hdc = GetDC(hWnd);

            GetTextMetrics(hdc,&tm);
            cxChar = tm.tmAveCharWidth;
            cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * cxChar/2;
            cyChar = tm.tmHeight + tm.tmExternalLeading;

            ReleaseDC(hWnd,hdc);

            iMaxWidth = 40 * cxChar + 22 * cxCaps;
            return 0;

        case WM_SIZE:
            cxClient = LOWORD(lParam);
            cyClient = HIWORD(lParam);

            si.cbSize = sizeof(si);
            si.fMask = SIF_RANGE | SIF_PAGE;
            si.nMin = 0;
            si.nMax = NUMLINES -1;
            si.nPage = cyClient / cyChar;

            SetScrollInfo(hWnd,SB_VERT,&si,TRUE);

            si.cbSize = sizeof(si);
            si.fMask = SIF_RANGE | SIF_PAGE;
            si.nMin = 0;
            si.nMax = 2 + iMaxWidth / cxChar;
            si.nPage = cxClient / cxChar;

            SetScrollInfo(hWnd,SB_HORZ,&si,TRUE);

            return 0;

        case WM_VSCROLL:
            si.cbSize = sizeof(si);
            si.fMask = SIF_ALL;

            GetScrollInfo(hWnd,SB_VERT,&si);

            iVertPos = si.nPos;

            switch(LOWORD(wParam))
            {
                case SB_TOP:
                    si.nPos = si.nMin;
                    break;

                case SB_BOTTOM:
                    si.nPos = si.nMax;
                    break;

                case SB_LINEUP:
                    si.nPos -= 1;
                    break;

                case SB_LINEDOWN:
                    si.nPos += 1;
                    break;

                case SB_PAGEUP:
                    si.nPos -= si.nPage;
                    break;

                case SB_PAGEDOWN:
                    si.nPos += si.nPage;
                    break;

                case SB_THUMBTRACK:
                    si.nPos = si.nTrackPos;
                    break;

                default:
                    break;                          

            }        

            si.fMask = SIF_POS;
            SetScrollInfo(hWnd,SB_VERT,&si,TRUE);
            GetScrollInfo(hWnd,SB_VERT,&si);

            if(si.nPos != iVertPos)
            {
                ScrollWindow(hWnd,0,cyChar*(iVertPos-si.nPos),NULL,NULL);
                UpdateWindow(hWnd);
            }

            return 0;

        case WM_HSCROLL:
            si.cbSize = sizeof(si);
            si.fMask = SIF_ALL;

            GetScrollInfo(hWnd,SB_HORZ,&si);
            iHorzPos = si.nPos;

            switch(LOWORD(wParam))
            {
                case SB_LINELEFT:
                    si.nPos -= 1;
                    break;

                case SB_LINERIGHT:
                    si.nPos += 1;
                    break;

                case SB_PAGELEFT:
                    si.nPos -= si.nPage;
                    break;

                case SB_PAGERIGHT:
                    si.nPos += si.nPage;
                    break;

                case SB_THUMBPOSITION:
                    si.nPos = si.nTrackPos;
                    break;

                default:
                    break;                    
            }    

            si.fMask = SIF_POS;
            SetScrollInfo(hWnd,SB_HORZ,&si,TRUE);
            GetScrollInfo(hWnd,SB_HORZ,&si);

            if(si.nPos != iHorzPos)
            {
                ScrollWindow(hWnd,cxChar*(iHorzPos-si.nPos),0,NULL,NULL);
            }

            return 0;

        case WM_PAINT:
            hdc = BeginPaint(hWnd,&ps);

            si.cbSize = sizeof(si);
            si.fMask = SIF_POS;
            GetScrollInfo(hWnd,SB_VERT,&si);
            iVertPos = si.nPos;

            GetScrollInfo(hWnd,SB_HORZ,&si);
            iHorzPos = si.nPos;

            iPaintBeg = max(0,iVertPos + ps.rcPaint.top/cyChar);
            iPaintEnd = min(NUMLINES-1,iVertPos + ps.rcPaint.bottom/cyChar);

            for(i = iPaintBeg ; i<= iPaintEnd;i++)
            {
                x = cxChar * (1 - iHorzPos);
                y = cyChar * (i - iVertPos);

                TextOut(hdc,x,y,sysmetrics[i].szLabel,lstrlen(sysmetrics[i].szLabel));

                TextOut(hdc,x + 22 * cxCaps, y, sysmetrics[i].szDesc,lstrlen(sysmetrics[i].szDesc));

                SetTextAlign(hdc, TA_RIGHT|TA_TOP);

                TextOut(hdc,x + 22 * cxCaps + 40 * cxChar , y, szBuffer,wsprintf(szBuffer,TEXT("%5d"),GetSystemMetrics(sysmetrics[i].iIndex)));

                SetTextAlign(hdc,TA_LEFT|TA_TOP);
            }    

            EndPaint(hWnd,&ps);
            return 0;

        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;    
    }

    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}