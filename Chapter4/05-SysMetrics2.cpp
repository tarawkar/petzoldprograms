#include <Windows.h>
#include "05-SysMetrics2.h"

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("SysMetrics2");
    HWND hWnd;
    MSG msg;
    WNDCLASS wndclass;

    wndclass.cbWndExtra = 0;
    wndclass.cbClsExtra = 0;
    
    wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
    wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);

    wndclass.hInstance = hInstance;

    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szAppName;
    wndclass.lpszMenuName = NULL;

    wndclass.style = CS_HREDRAW | CS_VREDRAW;

    if(!(RegisterClass(&wndclass)))
    {
        MessageBox(NULL,TEXT("Failed to Register class"),TEXT("Error"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    hWnd = CreateWindow(
        szAppName,
        TEXT("System Metrics 2 - With scroll bars"),
        WS_OVERLAPPEDWINDOW | WS_VSCROLL,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    ShowWindow(hWnd,iCmdShow);
    UpdateWindow(hWnd);

    while((GetMessage(&msg,NULL,0,0)))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
    static int cxChar,cyChar,cxCaps,cyClient,iVsScrollPos;
    HDC hdc;
    int i,y;
    PAINTSTRUCT ps;
    TCHAR szBuffer[10];
    TEXTMETRIC tm;

    switch (uMsg)
    {
        case WM_CREATE:
            hdc = GetDC(hWnd);
            GetTextMetrics(hdc,&tm);
            cxChar = tm.tmAveCharWidth;
            cxCaps = (tm.tmPitchAndFamily & 1 ? 3:2) * cxChar /2;
            cyChar = tm.tmHeight + tm.tmExternalLeading;

            ReleaseDC(hWnd,hdc);

            SetScrollRange(hWnd,SB_VERT,0,NUMLINES-1,FALSE);
            SetScrollPos(hWnd,SB_VERT,iVsScrollPos,TRUE);
            return 0;

        case WM_SIZE:
            cyClient = HIWORD(lParam);
            return 0;

        case WM_VSCROLL:
            switch(LOWORD(wParam))
            {
                case SB_LINEUP:
                    iVsScrollPos -= 1;
                    break;

                case SB_LINEDOWN:
                    iVsScrollPos +=1;
                    break;

                case SB_PAGEUP:
                    iVsScrollPos -= cyClient/cyChar;
                    break;

                case SB_PAGEDOWN:
                    iVsScrollPos += cyClient/cyChar;
                    break;

                case SB_THUMBPOSITION:
                    iVsScrollPos = HIWORD(wParam);
                    break;

                default: 
                    break;                    
            }      

            iVsScrollPos = max(0,min(iVsScrollPos,NUMLINES-1));

            if(iVsScrollPos != GetScrollPos(hWnd,SB_VERT))
            {
                SetScrollPos(hWnd,SB_VERT,iVsScrollPos,TRUE);
                InvalidateRect(hWnd,NULL,TRUE);
            }  

            return 0;

        case WM_PAINT:
            hdc = BeginPaint(hWnd,&ps);

            for(i=0;i<NUMLINES;i++)
            {
                y = cyChar * (i-iVsScrollPos);

                TextOut(hdc,0,y,sysmetrics[i].szLabel,lstrlen(sysmetrics[i].szLabel));

                TextOut(hdc,22*cxCaps,y,sysmetrics[i].szDesc,lstrlen(sysmetrics[i].szDesc));

                SetTextAlign(hdc,TA_RIGHT|TA_TOP);

                TextOut(hdc,22*cxCaps + 40*cxChar,y,szBuffer,wsprintf(szBuffer,TEXT("%5d"),GetSystemMetrics(sysmetrics[i].iIndex)));

                SetTextAlign(hdc,TA_LEFT|TA_TOP);
            }    

            EndPaint(hWnd,&ps);
            return 0;

        case WM_DESTROY:
            PostQuitMessage(0) ;
            return 0;   
    }

    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}