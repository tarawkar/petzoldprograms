/*Master Include file. 
Includes:
1. WINDEF.H -> Basic type definations.
2. WININT.H -> Type definations for Unicode support
3. WINBASE.H -> Kernal functions
4. WINUSER.H -> User Interface functions
5. WINGDI.H -> Graphics Device Interface functions*/
#include <Windows.h>

//Entry point documented in WINBASE.H
/*
Hungarian Notation -> preface the variable name with prefix showing variable's datatype.
eg. sz = zero terminated string, i = int
LPSTR = Long pointer to string (artifact of Windows-16 bit programing)

WINAPI - 
#define WINAPI __stdcall = pascal calling convention. Callee will clear the stack

Parameters:
hInstance = Handle to instance. Like a process id. Required by many WIN32 API. Hence given by OS
Handle = number that an application uses to identify something UNIQUELY.  

hPrevInstance = In early versions of Windows, jewha 3 notepad che instances concurrantly run hoyche, tewha text section share karayche. Program can check if other 
instances are running and then it can skip some chores and tranfer data from previous data area to my data area. Only for legacy purpose. Value = NULL

cmdLine = Command line arguments

iCmdShow = Flags of how the program should be initially displayed.

Return value:
int
*/
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    /*Displays a message box. 
    Parameters:
    Window handle
    Text string that appears in the body of message box
    Text string that appears in the caption bar of message box => Both are given in TEXT macro if you "want to be ready" to convert your program into Unicode character set
    Combination of constants
    
    Return value:
    IDOK or 1
    */
    
    MessageBox(NULL,TEXT("Hello, Windows 10"),TEXT("Hello Msg"),0);
    return 0;
}