#include <Windows.h>

#define NUMLINES ((int)(sizeof devcaps / sizeof devcaps[0]))

struct 
{
    int iIndex;
    TCHAR* szLabel;
    TCHAR* szDesc;
} devcaps [] = 
{
    HORZSIZE,   TEXT("HORZSIZE"),   TEXT("Width in mm"),
    VERTSIZE,   TEXT("VERTSIZE"),   TEXT("Height in mm"),
    HORZRES,    TEXT("HORZRES"),    TEXT("Width in pixels"),
    VERTRES,    TEXT("VERTRES"),    TEXT("Height in raster lines"),
    BITSPIXEL,  TEXT("BITSPIXEL"),  TEXT("Color bits per pixels"),
    PLANES,     TEXT("PLANES"),     TEXT("No of color planes"),
    NUMBRUSHES, TEXT("NUMBRUSHES"), TEXT("No of device brushes"),
    NUMPENS,    TEXT("NUMPENS"),    TEXT("No of device pens"),
    NUMMARKERS, TEXT("NUMMARKERS"), TEXT("No of device markers"),
    NUMFONTS,   TEXT("NUMFONTS"),   TEXT("No of device fonts"),
    NUMCOLORS,  TEXT("NUMCOLORS"),  TEXT("No of device colors"),
    PDEVICESIZE,TEXT("PDEVICESIZE"),TEXT("Size of device structure"),
    ASPECTX,    TEXT("ASPECTX"),    TEXT("Relative width of pixels"),
    ASPECTY,    TEXT("ASPECTY"),    TEXT("Relative height of pixels"),
    ASPECTXY,   TEXT("ASPECTXY"),   TEXT("Relative diagonal of pixel"),
    LOGPIXELSX, TEXT("LOGPIXELSX"), TEXT("Horizontal dots per inch"),
    LOGPIXELSY, TEXT("LOGPIXELSY"), TEXT("Vertical dots per inch"),
    SIZEPALETTE,TEXT("SIZEPALETTE"),TEXT("No of palatte entries"),
    NUMRESERVED,TEXT("NUMRESERVED"),TEXT("Reserved palatte entries"),
    COLORRES,   TEXT("COLORRES"),   TEXT("Actual color resolution")  
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg,WPARAM wParam,LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("devcaps1");
    HWND hWnd;
    MSG msg;
    WNDCLASS wndclass;

    wndclass.cbWndExtra = 0;
    wndclass.cbClsExtra = 0;

    wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
    
    wndclass.hInstance = hInstance;

    wndclass.lpfnWndProc = WndProc;
    wndclass.lpszClassName = szAppName;
    wndclass.lpszMenuName = NULL;

    wndclass.style = CS_HREDRAW | CS_VREDRAW;

    if(!(RegisterClass(&wndclass)))
    {
        MessageBox(NULL,TEXT("Cannot register class"),TEXT("Error"),MB_OK);
        return EXIT_FAILURE;
    }

    hWnd = CreateWindow(
        szAppName,
        TEXT("Devcaps 1 - Display device capabilities"),
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    ShowWindow(hWnd,iCmdShow);
    UpdateWindow(hWnd);

    while(GetMessage(&msg,NULL,0,0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }    

    return msg.wParam;

}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg,WPARAM wParam,LPARAM lParam)
{
    static int cxChar,cyChar,cxCaps;
    TCHAR szBuffer[10];
    HDC hdc;
    int i;
    PAINTSTRUCT ps;
    TEXTMETRIC tm;

    switch(uMsg)
    {
        case WM_CREATE:
            hdc = GetDC(hWnd);

            GetTextMetrics(hdc,&tm);
            cxChar = tm.tmAveCharWidth;
            cyChar = tm.tmHeight + tm.tmExternalLeading;
            cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * cxChar /2;

            ReleaseDC(hWnd,hdc);
            return 0;

        case WM_PAINT:
            hdc = BeginPaint(hWnd,&ps);

            for(i = 0; i < NUMLINES; i++)
            {
                TextOut(hdc,0,cyChar * i,devcaps[i].szLabel,lstrlen(devcaps[i].szLabel));

                TextOut(hdc,14 * cxCaps,cyChar * i,devcaps[i].szDesc,lstrlen(devcaps[i].szDesc));

                SetTextAlign(hdc, TA_RIGHT|TA_TOP);

                TextOut(hdc, 14 * cxCaps + 35 * cxChar, cyChar * i,szBuffer,wsprintf(szBuffer,TEXT("%5d"),GetDeviceCaps(hdc,devcaps[i].iIndex)));

                SetTextAlign(hdc, TA_LEFT|TA_TOP);
            }    

            EndPaint(hWnd,&ps);
            return 0;

        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;    
    }

    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}